debianutils (5.3-1) unstable; urgency=medium

  * tempfile has been removed and replaced by mktemp in GNU coreutils
    mktemp (see its manpage for different options).  Note that any use
    of tempfile in scripts or programs must be updated.  Failure to do
    so could even have security implications.
  * The 'which' utility will be removed in the future.  Shell scripts
    often use it to check whether a command is available.  A more
    standard way to do this is with 'command -v'; for example:
      if command -v update-icon-caches >/dev/null; then
        update-icon-caches /usr/share/icons/...
      fi
    '2>/dev/null' is unnecessary when using 'command': POSIX says "no
    output shall be written" if the command isn't found.  It's also
    unnecessary for the debianutils version of 'which', and hides the
    deprecation warning.

 -- Clint Adams <clint@debian.org>  Fri, 20 Aug 2021 07:22:18 -0400
